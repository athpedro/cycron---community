<?php

if (!file_exists('madeline.php')) {
    copy('https://phar.madelineproto.xyz/madeline.php', 'madeline.php');
}
include 'madeline.php';

$MadelineProto = new \danog\MadelineProto\API('session.madeline');

class EventHandler extends \danog\MadelineProto\EventHandler
{
    public function __construct($MadelineProto)
    {
        parent::__construct($MadelineProto);
    }
    public function onUpdateSomethingElse($update)
    {
        // See the docs for a full list of updates: http://docs.madelineproto.xyz/API_docs/types/Update.html
    }
    public function onUpdateNewChannelMessage($update)
    {
        yield $this->onUpdateNewMessage($update);
    }
    public function onUpdateNewMessage($update)
    {
       
        if ($update['_'] == 'updateNewChannelMessage' && $update['message'] && $update['message']['to_id']) {
          $channel_id = $update['message']['to_id']['channel_id'];
          $message = $update['message']['message'];

          $url = 'http://127.0.0.1:8181/saveNotification/2014222NOTIFICATION';

          $fields = array(
              'message' => $message,
          );
          
          $fields_string = '';
          
          foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }

          rtrim($fields_string, '&');
          
          //open connection
          $ch = curl_init();
          
          //set the url, number of POST vars, POST data
          curl_setopt($ch,CURLOPT_URL, $url);
          curl_setopt($ch,CURLOPT_POST, count($fields));
          curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
          
          //execute post
          $result = curl_exec($ch);
          
          //close connection
          curl_close($ch);
        
          var_dump($result);
          echo "\n";
          var_dump($message);
          echo "\n";
          echo "\n";
        }

    }
}

$MadelineProto->async(true);

$MadelineProto->loop(function () use ($MadelineProto) {
    yield $MadelineProto->start();
    yield $MadelineProto->setEventHandler('\EventHandler');
});

$MadelineProto->loop();