<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Post;
use App\User;

class postsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$images = [
    		"32319678963_f94d7b26cc_o-860x430",
    		"bitcoin-computer-e1490855093783-860x430",
    		"Brian-Armstrong-860x430",
    		"head-of-trading-circle-860x430",
    		"IMG_0140-860x430",
    		"LTC-and-USD-860x430",
    		"shutterstock_176573198-1-860x430"
    	];

    	DB::table('users')->insert([
    		'id' => 1,
            'name' => 'Nicolas Cane',
			'email' => 'admin@mail.com',
			'role' => 1,
            'password' => \Hash::make('123456789'),
        ]);

        $faker = Faker::create();

    	foreach (range(1, 200) as $index) {
	        $post = new Post([
	            'image' => '/images/posts/'.$images[rand(0, count($images)-1)].'.jpg',
	            'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
	            'description' => $faker->paragraph($nbSentences = 20, $variableNbSentences = true),
	            'published_by' => 1,
	            'number_of_visits' => rand(0,10000),
	            'status' => 1,
	            'important' => rand(0,1),
	        ]);

            $post->save();
		}

    	foreach (range(1, 50) as $index) {

	        DB::table('users')->insert([
				'name' => $faker->name,
				'email' => $faker->email,
				'role' => 0,
				'password' => \Hash::make('123456789'),
	        ]);
			
		}
    }
}
