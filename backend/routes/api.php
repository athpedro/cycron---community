<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
       
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');
        
        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');
        
        $api->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
        $api->post('refresh', 'App\\Api\\V1\\Controllers\\RefreshController@refresh');
        
        $api->get('me', 'App\\Api\\V1\\Controllers\\UserController@me');
    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {

        $api->get('posts/all', 'App\\Api\\V1\\Controllers\\PostController@all');
        $api->get('posts', 'App\\Api\\V1\\Controllers\\PostController@index');
        $api->post('posts/store', 'App\\Api\\V1\\Controllers\\PostController@store');
        $api->post('posts/update/{id}', 'App\\Api\\V1\\Controllers\\PostController@update');
        $api->delete('posts/destroy/{id}', 'App\\Api\\V1\\Controllers\\PostController@destroy');
        $api->post('posts/image/content', 'App\\Api\\V1\\Controllers\\PostController@setImagenContent');
        $api->get('posts/get/{id}', 'App\\Api\\V1\\Controllers\\PostController@get_by_id');

        $api->get('users', 'App\\Api\\V1\\Controllers\\UserController@index');
        $api->post('users/update/{id}', 'App\\Api\\V1\\Controllers\\UserController@update');
        $api->delete('users/destroy/{id}', 'App\\Api\\V1\\Controllers\\UserController@destroy');

        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
    });



});
