<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

Route::get('reset_password/{token}', ['as' => 'password.reset', function($token)
{
    // implement your reset password route here!
}]);

Route::get('/', function () {
    return view('welcome');
});

Route::get('/posts/get/{id}', function ($id) {
    return response()->json(\App\Post::find($id));
});

Route::post('/saveNotification/{token}', function ($token, Request $request) {
    if ($token == '2014222NOTIFICATION') {
        $message = new \App\TelagramMessage;
        $message->message = $request->message;
        $message->save();
        
        return response()->json(['ok']);
    }
});

Route::get('/getSignals', function (Request $request) {
    return response()->json(\App\TelagramMessage::all());
});
