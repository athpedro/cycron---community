<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Api\V1\Requests\UserUpdateResquest;
use App\Api\V1\Requests\UserDestroyResquest;


class UserController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth', []);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(Auth::guard()->user());
    }

    public function index(Request $request)
    {
        try {
            $paginator = User::paginate($request->get('limit', config('app.pagination_limit', 5)));
            if ($request->has('limit')) {
                $paginator->appends('limit', $request->get('limit'));
            }

            return response()->json($paginator, 200);
       } catch (\Exception $ex) { // Anything that went wrong
            $error = [
                "message"     => "Internal error",
                "status_code" => 500,
                "errors"      => ["error" => $ex->getMessage()]
            ];
            return response()->json($error, 500);
        }
    }

    public function update(UserUpdateResquest $request, $id)
    {
        $data = $request->only("email","name");
        User::whereId($id)->update($data);
        $user = User::find($id);
        return response()->json($user, 201);
    }

    public function destroy(UserDestroyResquest $request, $id)
    {
        User::find($id)->forceDelete($id);
        return response()->json(["message" => "Success delete"], 200);
    }
}
