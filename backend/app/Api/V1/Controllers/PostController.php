<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Api\V1\Requests\PostStoreRequest;
use App\Api\V1\Requests\PostDestroyResquest;
use App\Api\V1\Requests\PostUpdateResquest;
use Auth;
use Facebook\Facebook;
use Facebook\FileUpload\FacebookFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;


use App\Post;

class PostController extends Controller
{
    /**
     * @var User
     */
    protected $model;

    /**
     * @var array
     */
    protected $validMimes = [
        'image/jpeg' => [
            'type' => 'image',
            'extension' => 'jpeg',
        ],
        'image/jpg' => [
            'type' => 'image',
            'extension' => 'jpg',
        ],
        'image/png' => [
            'type'      => 'image',
            'extension' => 'png',
        ],
    ];

    /**
     * Create a new PostController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth', []);
    }

    public function all()
    {
        return response()->json(Post::orderBy('created_at', 'desc')->get());
    }

    public function index(Request $request)
    {
        try {
            $paginator = Post::orderBy('created_at', 'desc')->paginate($request->get('limit', config('app.pagination_limit', 5)));
            if ($request->has('limit')) {
                $paginator->appends('limit', $request->get('limit'));
            }
            return response()->json($paginator, 200);
       } catch (\Exception $ex) { // Anything that went wrong
            $error = [
                "message"     => "Internal error",
                "status_code" => 500,
                "errors"      => ["error" => $ex->getMessage()]
            ];
            return response()->json($error, 500);
        }
    }

    public function get_by_id($id)
    {
        return response()->json(Post::find($id));
    }

    public function store(PostStoreRequest $request)
    {
        $imageName = md5($request->file('image')->getClientOriginalName()).".".$request->file('image')->getClientOriginalExtension();
        $path = '/images/posts/';
        $request->file('image')->move( public_path() . $path, $imageName );

        $data                     = $request->only("title","description");
        $data['image']            = $path.$imageName;

        $post = Post::create($data);

        if (!($request->input('public_facebook') == "0" || $request->input('public_facebook') == "false")) {
            $fb = new Facebook([
                'app_id'     => config('app.facebook.app_id'),
                'app_secret' => config('app.facebook.app_secret')
            ]);

            $linkData = [
                'message' => $post->title . ': ' . $post->description,
                'link' => asset('/images/posts/'.$post->image)
            ];

            try {
                $response = $fb->post(config('app.facebook.page_feed'), $linkData, config('app.facebook.app_access_token'));
            } catch(Facebook\Exceptions\FacebookResponseException $e) {
            } catch(Facebook\Exceptions\FacebookSDKException $e) {
            }
        }

        return response()->json($post, 201);
    }

    public function update(PostUpdateResquest $request, $id)
    {

        $imageName = "";
        if ($request->file("image")) {
            $imageName = md5($request->file('image')->getClientOriginalName()).".".$request->file('image')->getClientOriginalExtension();
            $path = '/images/posts/';
            $request->file('image')->move( public_path() . $path, $imageName );
        }

        $data = $request->only("title","description","status");

        if ($imageName) {
            $data["image"] = $path.$imageName;
        }
        $post = Post::whereId($id)->update($data);
        $post = Post::find($id);
        return response()->json($post, 201);
    }


    public function destroy(PostDestroyResquest $request, $id)
    {
        $post = Post::find($id);
        try {
            unlink(public_path($post->image));
        } catch (\Exception $e) {
        }
        $post->forceDelete();
        return response()->json(["message" => "Success delete"], 200);
    }


    public function setImagenContent(Request $request)
    {
        $image = $request->image;
        $pos    = strpos($image, ';');
        $type   = explode(':', substr($image, 0, $pos))[1];

        $image = str_replace('data:'.$type.';base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $path = '/images/posts-content/';
        $path   .= md5(str_random(16).date('U')).".".$this->validMimes[$type]['extension'];
        Storage::disk('public')->put($path, base64_decode($image));

        return response()->json(["default" => asset($path)]);
    }
}
