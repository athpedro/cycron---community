<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

use Illuminate\Validation\Rule;

class UserUpdateResquest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all($key = null)
    {
        $data = parent::all($key);
        $data["id"] = $this->route("id");
        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore = Rule::unique('users')->ignore($this->route('id'), 'id');
        return [
            "id"    => "integer|required|exists:users,id",
            "name"  => "sometimes|required|min:3|max:255",
            "email" =>  ['sometimes','required','email','min:10','max:180',$ignore],
        ];
    }
}
