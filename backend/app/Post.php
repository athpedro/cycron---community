<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

use Auth;

class Post extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'status',
        'published_by',
        'image',
        'number_of_visits',
    ];

    /**
     * @param array $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public static function create(array $attributes = [])
    {

        $attributes['status']           = 1;
        $attributes['number_of_visits'] = 0;

        if (!isset($attributes['published_by'])) {
            $attributes['published_by'] = Auth::guard()->user()->id;
        }

        $model = static::query()->create($attributes);

        return $model;
    }

}
